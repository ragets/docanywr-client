import { Patient } from './patient';

export class AddPatientResponse {

    constructor(
        public isSuccess: boolean,
        public msg: string,
        public data : Patient
    ) {}
}
