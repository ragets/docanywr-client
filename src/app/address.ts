export class Address {
    constructor(
        public id : number,
        public addressLine1: string,
        public addressLine2: string,
        public city:string,
        public country:string,
        public patientId : number
      ) {  }
}
