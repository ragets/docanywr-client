import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Optional } from '@angular/core';
import { Inject } from '@angular/core';
import { Patient } from '../patient';
import { Address } from '../address';


@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.css']
})
export class DialogBoxComponent implements OnInit {


  patient = new Patient(0,'' ,'','',[]);
  action:string;
  local_data:any;
  parmanentDelete = false;

  address : Address[] = [{id:0,addressLine1:"",addressLine2:"",city:"",country:"",patientId:0},
  {id:0,addressLine1:"",addressLine2:"",city:"",country:"",patientId:0}]
  
  constructor(public dialogRef: MatDialogRef<DialogBoxComponent> 
  ,@Optional() @Inject(MAT_DIALOG_DATA) public data: Patient){ 
    this.local_data = {...data};
    this.action = this.local_data.action;
  }

  

  ngOnInit() {
  }

  doAction(){
    console.log("------",this.local_data, this.parmanentDelete)
    if(this.local_data.action == 'Add'){
      this.local_data.address = this.address;
    }
    this.local_data.parmanentDelete = this.parmanentDelete;
    this.dialogRef.close({event:this.action,data:this.local_data});
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

}
