import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { ThrowStmt } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class FirbaseServiceService {

  newUser:any;

  private eventAuthError = new BehaviorSubject<string>("");
  eventAuthError$ = this.eventAuthError.asObservable();

  constructor(
    private firebaseAuth : AngularFireAuth,
    private db:AngularFirestore,
    private router : Router
  ) { }


 createFireBaseUser(user){
this.firebaseAuth.createUserWithEmailAndPassword(String(user.emailId) , user.password).then(
  userCredential => {
    this.newUser = user;
    console.log(userCredential);
    userCredential.user.updateProfile(
      {
        displayName : user.name
      }
    );
    this.insertUserData(userCredential).then(() => { 
        console.log("user-------",userCredential)
        this.router.navigate(['/']);
    });
  }).catch(error =>{
    console.log(error);
    this.eventAuthError.next(error);
  });
 }

 insertUserData(userCredential: firebase.auth.UserCredential){
   return this.db.doc('Users/$(userCredential.user.uid)').set({
     email :this.newUser.emailId,
     name : this.newUser.name,
     password : this.newUser.password
   })
 }

 login( email: string, password: string) {
  this.firebaseAuth.signInWithEmailAndPassword(email, password)
    .catch(error => {
      this.eventAuthError.next(error);
    })
    .then(userCredential => {
      if(userCredential) {
        console.log("usr ------",userCredential)
        this.router.navigate(['/home']);
      }
    })
}


getUserState() {
  return this.firebaseAuth.authState;
}

logout() {
  this.router.navigate(['/']);
  return this.firebaseAuth.signOut();
}

}
