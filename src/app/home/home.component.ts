import { Component, OnInit } from '@angular/core';
import { FirbaseServiceService } from '../firbase-service.service'
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Patient } from '../patient';
import { DialogBoxComponent } from '../dialog-box/dialog-box.component'
import { ViewChild } from '@angular/core';
import { MatTable} from '@angular/material'
import {MatPaginator,MatTableDataSource} from '@angular/material';
import { PatientServiceService } from '../patient-service.service'
import { BehaviorSubject } from 'rxjs';




@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private firbaseServiceService: FirbaseServiceService,
    private router: Router, private matDialog: MatDialog, private patientService: PatientServiceService) { }


  @ViewChild(MatTable, { static: true }) table: MatTable<any>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  private eventAuthError = new BehaviorSubject<string>("");
  eventAuthError$ = this.eventAuthError.asObservable();

  user: firebase.User;

  authError: any;
  public idToken : any;


  ELEMENT_DATA: Patient[] = [];
  dataSource : MatTableDataSource<Patient>;

  displayedColumns: string[] = ['id','firstName', 'lastName', 'contactNumber', 'action'];
  

  ngOnInit() {
    this.firbaseServiceService.getUserState()
      .subscribe(user => {
        this.user = user;
        user.getIdToken(/* forceRefresh */ true).then(function(idToken){
          localStorage.setItem('idToken',idToken);
        })
      })
    this.getAllPatient();
   // this.dataSource.paginator = this.paginator;
   this.dataSource = new MatTableDataSource<Patient>(this.ELEMENT_DATA);
  
  }

  ngAfterViewInit(): void {   
    //this.dataSource.paginator = this.paginator;
  }

  logout() {
    this.firbaseServiceService.logout();
  }


  openDialog(action, obj) {
    obj.action = action;
    const dialogRef = this.matDialog.open(DialogBoxComponent, {
      width: '500px',
      data: obj
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result.event == 'Add') {
           this.addPatient(result.data)
      } else if (result.event == 'Update') {
        console.log(result.data)
        this.updateRow(result.data)
      } else if (result.event == 'Delete') {
        console.log('The dialog was closed',result.data.parmanentDelete,result.data);
        this.deleteRow(result.data)
      }

    });

  }


  deleteRow(data){
  const index: number = this.ELEMENT_DATA.indexOf(data.id);
  this.ELEMENT_DATA.splice(index ,1);
  this.table.renderRows();
  this.dataSource.paginator = this.paginator;
  if(data.parmanentDelete){
     this.deletePatient(data);
  }

  }


  updateRow(data) {
    this.ELEMENT_DATA = this.ELEMENT_DATA.filter((value, key) => {
      if (value.id === data.id) {
        value.firstName = data.firstName;
        value.lastName = data.lastName;
        value.contactNumber = data.contactNumber;
        value.address = data.address;
        this.updatePatient(data);
      }
      return true;
    });
  }


  addPatient(data){
    this.patientService.post("http://ec2-3-134-111-87.us-east-2.compute.amazonaws.com:8080/patient",data).subscribe((res) => {
      if (res.isSuccess) {
          this.ELEMENT_DATA.push(new Patient(res.data.id, res.data.firstName, res.data.lastName, res.data.contactNumber, res.data.address))
          this.table.renderRows();
          this.dataSource.paginator = this.paginator;
      }
    },error => {
      const errorMsg = 'Something Went Worng';
      this.eventAuthError.next(errorMsg);
    })
  }

  

  getAllPatient() {
    this.patientService.get("http://ec2-3-134-111-87.us-east-2.compute.amazonaws.com:8080/getAllPatient").subscribe((res) => {
      if (res.isSuccess) {
        console.log(res.data)
        res.data.map(d => {
          this.ELEMENT_DATA.push(new Patient(d.id, d.firstName, d.lastName, d.contactNumber, d.address))
        })
        this.table.renderRows();
        this.dataSource.paginator = this.paginator;
      }
    }, error => {
      const errorMsg = 'Something Went Worng';
      this.eventAuthError.next(errorMsg);
    });
  }


  updatePatient(data) {
    this.patientService.put("http://ec2-3-134-111-87.us-east-2.compute.amazonaws.com:8080/patient",data).subscribe((res) => {
      if (res.isSuccess) {
        console.log("Data updated successfully");
      }
    }, error => {
      const errorMsg = 'Something Went Worng';
      this.eventAuthError.next(errorMsg);
    });
  }

  
  deletePatient(data) {
    this.patientService.put("http://ec2-3-134-111-87.us-east-2.compute.amazonaws.com:8080/deletePatient",data).subscribe((res) => {
      if (res.isSuccess) {
        console.log("Data delete successfully");
      }
    }, error => {
      const errorMsg = 'Something Went Worng';
      this.eventAuthError.next(errorMsg);
    });
  }

  logOut(){
    console.log("logging Out");
    this.logout();
    localStorage.clear();
  }

}
