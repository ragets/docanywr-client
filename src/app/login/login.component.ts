import { Component, OnInit } from '@angular/core';
import {FirbaseServiceService} from '../firbase-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private firbaseServiceService : FirbaseServiceService) { }

  authError: any;

  ngOnInit() {
    this.firbaseServiceService.eventAuthError$.subscribe( data => {
      this.authError = data;
    })
  }
  onSubmit(form){
     console.log("-------------------",form.value)
     this.login(form);
  }


  login(form){
    this.firbaseServiceService.login(String(form.value.emailId),form.value.password)
  }

}
