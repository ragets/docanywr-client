import { Patient } from './patient';

export class PatientResponse {
    constructor(
        public isSuccess: boolean,
        public msg: string,
        public data : Patient[]
    ) {}
}
