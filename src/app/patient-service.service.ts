import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders,HttpErrorResponse} from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { PatientResponse } from './patient-response';
import { AddPatientResponse } from './add-patient-response';

@Injectable({
  providedIn: 'root'
})
export class PatientServiceService {

  constructor(public httpClient: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }).append( 'Accept','application/json').append('X-Xsrf-Token',localStorage.getItem('idToken'))
  };
  
  get(url : any) {
   return this.httpClient.get<PatientResponse>(url,this.httpOptions).pipe(catchError(this.errorHandler));
  }

  put(url : any, data:any) {
    return this.httpClient.put<PatientResponse>(url, data,this.httpOptions).pipe(catchError(this.errorHandler));
   }

   post(url : any, data:any) {
    return this.httpClient.post<AddPatientResponse>(url, data,this.httpOptions).pipe(catchError(this.errorHandler));
   }


  errorHandler(error: HttpErrorResponse) {
    return throwError(error);
 }
}
