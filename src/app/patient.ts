import {Address}  from './address'
export class Patient {
    constructor(
        public id : number,
        public firstName: string,
        public lastName: string,
        public contactNumber:string,
        public address:Address[]
      ) {  }

}
