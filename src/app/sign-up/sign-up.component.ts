import { Component, OnInit } from '@angular/core';
import {FirbaseServiceService} from '../firbase-service.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  authError: any;

  constructor(private firebaseAuthService : FirbaseServiceService) { }
 
  ngOnInit() {
    this.firebaseAuthService.eventAuthError$.subscribe( data => {
      this.authError = data;
    })
  }


  onSubmit(form){
    console.log("-------------------",form.value);
        this.createUser(form);
  }

  createUser(form) {
    console.log("-------------------",form.value);
this.firebaseAuthService.createFireBaseUser(form.value);
  }

}
