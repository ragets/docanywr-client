// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
   firebaseConfig : {
    apiKey: "AIzaSyBPxpqsD0HwWFf_HYR2nqz05ccbMomDTKc",
    authDomain: "docanywr.firebaseapp.com",
    databaseURL: "https://docanywr.firebaseio.com",
    projectId: "docanywr",
    storageBucket: "docanywr.appspot.com",
    messagingSenderId: "1033777368237",
    appId: "1:1033777368237:web:d8e7e2441ba3310303ca24",
    measurementId: "G-WDT4HLVPN2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
